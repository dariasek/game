import { callApi } from '../helpers/apiHelper';
import { getFighterById } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
    const fighterDetails = getFighterById(`details/fighter/${id}.json`);
    
    return fighterDetails;
  }
}

export const fighterService = new FighterService();
