import {showModal} from './modal.js';

export function showWinnerModal(fighter) {

  // call showModal function 

  let winner = {
    title: 'Winner',
    bodyElement: fighter['name'],
  }

  showModal(winner);

}
