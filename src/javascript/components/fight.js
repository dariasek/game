import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  const firstFighterHealthStart = firstFighter['health'];
  const secondFighterHealthStart = secondFighter['health'];

  let firstFighterHealth = firstFighter['health'];
  let secondFighterHealth = secondFighter['health'];

  let prevLetter;
  let winner;

  let  keyStore = {}; 
  let allowQWE = true;
  let allowUIO = true;

  let firstHealthIndicator = document.getElementById('left-fighter-indicator');
  let secondHealthIndicator = document.getElementById('right-fighter-indicator');

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    document.onkeydown = function(event){ 
      if (firstFighterHealth > 0 && secondFighterHealth > 0){

        //qwe and uio key-set 
        onkeydown = onkeyup = function(e){
            keyStore[e.key] = e.type == 'keydown';

            if (keyStore['q'] && keyStore['w'] && keyStore['e']){
              QWE();
            } else if (keyStore['u'] && keyStore['i'] && keyStore['o']){
              UIO();
            }    
        }

        function QWE() {
          if (allowQWE === true){
            secondFighterHealth -= 2*firstFighter['attack'];
            secondHealthIndicator.style.width = `${(100*secondFighterHealth)/secondFighterHealthStart}%`;

            allowQWE = false;
            setTimeout(()=> {
              allowQWE = true}, 10000);
          }
        }

        function UIO() {

          if (allowUIO === true){
            firstFighterHealth -= 2*secondFighter['attack'];
            firstHealthIndicator.style.width = `${(100*firstFighterHealth)/firstFighterHealthStart}%`;

            allowUIO = false;
            setTimeout(()=> {
              allowUIO = true}, 10000);
          }
        }

        switch (event.key) {
    
          case 'a': 
    
          if (prevLetter != 'l'){
            //first fighter kick second fighter
            secondFighterHealth -= getDamage(firstFighter, secondFighter);
            secondHealthIndicator.style.width = `${(100*secondFighterHealth)/secondFighterHealthStart}%`
          }
          break;
    
          case 'j':
            if (prevLetter != 'd'){
              //second fighter kick first fighter
              firstFighterHealth -= getDamage(secondFighter, firstFighter);
              firstHealthIndicator.style.width = `${(100*firstFighterHealth)/firstFighterHealthStart}%`;
            }
            break;
        }     
        prevLetter = event.key;
      } else {
        winner = firstFighterHealth > secondFighterHealth ? firstFighter : secondFighter;
        return resolve(winner);
      }
    
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage>0 ? damage : 0;

}

export function getHitPower(fighter) {
  // return hit power
  let attack = fighter['attack'];
  let criticalHitChance = Math.random() + 1 ;
  let hitPower = attack*criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  let defense = fighter['defense'];
  let dodgeChance = Math.random() + 1 ;
  let blockPower = defense * dodgeChance;
  return blockPower;
}
